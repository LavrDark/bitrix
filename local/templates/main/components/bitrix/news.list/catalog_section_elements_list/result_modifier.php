<?php
$request = Bitrix\Main\Context::getCurrent()->getRequest();
$section = $request->getQueryList()->toArray();

$arFilter = Array(
    'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
    'GLOBAL_ACTIVE'=>'Y',
    'CODE'=>$section['SECTION_CODE'],
);
$dbList = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);

$curSection = $dbList->GetNext();
$arResult['SECTION'] = $curSection;

if($arResult['SECTION']['IBLOCK_SECTION_ID']){
    $arFilter = Array(
        'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
        'GLOBAL_ACTIVE'=>'Y',
        'ID'=>$arResult['SECTION']['IBLOCK_SECTION_ID'],
    );
    $dbList = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
    $mainSection = $dbList->GetNext();
    $arResult['MAIN_SECTION'] = $mainSection;
    $APPLICATION->AddChainItem($arResult['MAIN_SECTION']['NAME'], '/catalog/'.$arResult['MAIN_SECTION']['CODE'].'/');
    $APPLICATION->AddChainItem($arResult['SECTION']['NAME'], '/catalog/'.$arResult['SECTION']['CODE'].'/');
} else {
    $arResult['MAIN_SECTION'] = $arResult['SECTION'];
    $APPLICATION->AddChainItem($arResult['MAIN_SECTION']['NAME'], '/catalog/'.$arResult['MAIN_SECTION']['CODE'].'/');
}


$arFilter = Array(
    'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
    'GLOBAL_ACTIVE'=>'Y',
    'SECTION_ID'=>$arResult['MAIN_SECTION']['ID'],
    'DEPTH_LEVEL'=>2,
);

$dbList = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
$section=[];
$section[0]['SECTION_CODE'] = $arResult['MAIN_SECTION']['CODE'].'/';
$section[0]['SECTION_NAME'] = "Все";
while($arrResult = $dbList->GetNext()){
    if($arrResult['ELEMENT_CNT']>0) {
        $section[$arrResult['ID']]['SECTION_CODE'] = $arrResult['CODE'] . '/';
        $section[$arrResult['ID']]['SECTION_NAME'] = $arrResult['NAME'];
    }
}

if(count($section) == 1){
    unset($section);
}

$arResult['SECTIONS'] = $section;


