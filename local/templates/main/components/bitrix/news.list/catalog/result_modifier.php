<?php


foreach ($arResult['ITEMS'] as $index => $item) {
    foreach ($item['PROPERTIES'] as $propertyName => $values) {
        if ($values['PROPERTY_TYPE'] === 'F') {
            $rsFile = CFile::GetByID($values['VALUE']);
            $arFile = $rsFile->Fetch();
            $arResult['ITEMS'][$index]['PROPERTIES'][$propertyName]['VALUE'] = $arFile;
        }
    }
}


