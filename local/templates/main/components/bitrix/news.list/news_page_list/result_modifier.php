<?php
$arFilter = Array(
    'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
    'GLOBAL_ACTIVE'=>'Y',
    'PROPERTY'=>Array('SRC'=>'https://%')
);
$dbList = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
$section=[];
$section[0]['SECTION_CODE'] = "";
$section[0]['SECTION_NAME'] = "Все";
do{
    $arrResult = $dbList->GetNext();
    if($arrResult) {
        $section[$arrResult['ID']]['SECTION_CODE'] = $arrResult['CODE'] . '/';
        $section[$arrResult['ID']]['SECTION_NAME'] = $arrResult['NAME'];
    }
    else{
        $error = "Nothing to show, please add the element to IB";
        $arrResult['ERROR_MESSAGE'] = $error;
        break;
    }
}while($arrResult);

$arResult['SECTIONS'] = $section;

foreach ($arResult['ITEMS'] as $index => $arItem) {
    $dateCreate = CIBlockFormatProperties::DateFormat(
        'j F Y',
        MakeTimeStamp(
            $arItem["DATE_CREATE"],
            CSite::GetDateFormat()
        )
    );
    $arResult['ITEMS'][$index]['DATE_CREATE'] = $dateCreate;
    $arResult['ITEMS'][$index]['SECTION_NAME'] = mb_strtolower($section[$arItem['IBLOCK_SECTION_ID']]['SECTION_NAME']);
}