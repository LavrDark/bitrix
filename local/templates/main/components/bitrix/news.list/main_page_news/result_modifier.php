<?php

$arFilter = Array(
    'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
    'GLOBAL_ACTIVE'=>'Y',
    'PROPERTY'=>Array('SRC'=>'https://%')
);

$dbList = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
$section=[];

while($arrResult = $dbList->GetNext())
{
    $section[$arrResult['ID']]['SECTION_CODE'] = $arrResult['CODE'] . '/';
    $section[$arrResult['ID']]['SECTION_NAME'] = $arrResult['NAME'];
}

$arResult['SECTIONS'] = $section;


foreach ($arResult['ITEMS'] as $index => $arItem) {
    $dateCreate = CIBlockFormatProperties::DateFormat(
        'j F Y',
        MakeTimeStamp(
            $arItem["DATE_CREATE"],
            CSite::GetDateFormat()
        )
    );
    $arResult['ITEMS'][$index]['DATE_CREATE'] = $dateCreate;
    $arResult['ITEMS'][$index]['SECTION_NAME'] = mb_strtolower($section[$arItem['IBLOCK_SECTION_ID']]['SECTION_NAME']);
    if(!empty($arItem["PREVIEW_PICTURE"]))
        $newsPic = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'],[1200,800], BX_RESIZE_IMAGE_EXACT);
    $arResult['ITEMS'][$index]['PREVIEW_PICTURE'] = $newsPic;
}
?>