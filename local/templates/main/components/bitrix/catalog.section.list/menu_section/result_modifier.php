<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['SECTIONS'] as $index => $SECTION) {
    if(!empty($SECTION['UF_CATALOG_MENU_IMG'])) {
        $menu_catalog_file = CFile::GetByID($SECTION['UF_CATALOG_MENU_IMG']);
        $menu_catalog_pic = $menu_catalog_file->Fetch();
        $arResult['SECTIONS'][$index]['UF_CATALOG_MENU_IMG'] = $menu_catalog_pic;
    }if(!empty($SECTION['UF_CATALOG_MENU_IMG_MOB'])) {
        $menu_catalog_file_mob = CFile::GetByID($SECTION['UF_CATALOG_MENU_IMG_MOB']);
        $menu_catalog_pic = $menu_catalog_file_mob->Fetch();
        $arResult['SECTIONS'][$index]['UF_CATALOG_MENU_IMG_MOB'] = $menu_catalog_pic;
    }
    if(!empty($SECTION['UF_CATALOG_TEXT_COLOR'])){
        $rsEnum = CUserFieldEnum::GetList([],['ID'=>$SECTION['UF_CATALOG_TEXT_COLOR']]);
        $arEnum = $rsEnum->GetNext();
        $arResult['SECTIONS'][$index]['UF_CATALOG_TEXT_COLOR']=$arEnum['VALUE'];
    }
}
?>