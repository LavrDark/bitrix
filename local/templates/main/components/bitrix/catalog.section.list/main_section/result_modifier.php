<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['SECTIONS'] as $index => $SECTION) {
    if(!empty($SECTION['DETAIL_PICTURE'])){
        $desc_file = CFile::GetByID($SECTION['DETAIL_PICTURE']);
        $menu_catalog_pic = $desc_file->Fetch();
        $arResult['SECTIONS'][$index]['DETAIL_PICTURE'] = $menu_catalog_pic;
    }
    if(!empty($SECTION['UF_CATALOG_TEXT_COLOR'])){
        $rsEnum = CUserFieldEnum::GetList([],['ID'=>$SECTION['UF_CATALOG_TEXT_COLOR']]);
        $arEnum = $rsEnum->GetNext();
        $arResult['SECTIONS'][$index]['UF_CATALOG_TEXT_COLOR']=$arEnum['VALUE'];
    }
}
?>