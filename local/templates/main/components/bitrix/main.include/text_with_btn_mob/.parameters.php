<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"INCLUDE_BTN_DESC" => Array(
		"NAME" => GetMessage("INCLUDE_BTN_DESC"),
		"TYPE" => "STRING",
	),
    "INCLUDE_BTN_URL" => Array(
        "NAME" => GetMessage("INCLUDE_BTN_URL"),
        "TYPE" => "STRING",
    ),
);
