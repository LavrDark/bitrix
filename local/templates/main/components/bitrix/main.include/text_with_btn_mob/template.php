<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if($arResult["FILE"] <> ''):?>
    <div class="main-traditions__text"><?include($arResult["FILE"]);?></div>
    <a class="main-traditions__button btn-hover_parent mobile" href="<?=$arParams['INCLUDE_BTN_URL']?>">
        <div class="btn-hover_circle"></div>
        <span>
             <?=$arResult['BTN_DESC']?>
        </span>
    </a>
<?endif;?>
