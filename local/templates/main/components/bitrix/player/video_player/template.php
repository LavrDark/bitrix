<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="popup-video__inner">
    <div class="popup-video__video">
        <video class="zp-video" controls="controls" width="100%" height="100%" poster="<?=$arParams['PREVIEW'];?>" preload="none">
            <source src="<?=$arResult['PATH'];?>">
        </video>
    </div>
</div>
