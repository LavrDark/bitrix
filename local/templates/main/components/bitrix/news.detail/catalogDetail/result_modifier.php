<?php
foreach ($arResult['PROPERTIES']['detailPicture']['VALUE'] as $index => $picture) {
    if(!empty($picture)) {
        $pic = CFile::ResizeImageGet($picture, [1248, 1248], BX_RESIZE_IMAGE_EXACT);
        $arResult['PROPERTIES']['detailPicture']['VALUE'][$index] = $pic;
    }
}

$request = Bitrix\Main\Context::getCurrent()->getRequest();
$section = $request->getQueryList()->toArray();

$arFilter = Array(
    'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
    'GLOBAL_ACTIVE'=>'Y',
    'CODE'=>$section['SECTION_CODE'],
);
$dbList = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);

$curSection = $dbList->GetNext();
$arResult['SECTION'] = $curSection;

if($arResult['SECTION']['IBLOCK_SECTION_ID']){
    $arFilter = Array(
        'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
        'GLOBAL_ACTIVE'=>'Y',
        'ID'=>$arResult['SECTION']['IBLOCK_SECTION_ID'],
    );
    $dbList = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
    $mainSection = $dbList->GetNext();
    $arResult['MAIN_SECTION'] = $mainSection;
    $APPLICATION->AddChainItem($arResult['MAIN_SECTION']['NAME'], '/catalog/'.$arResult['MAIN_SECTION']['CODE'].'/');
    $APPLICATION->AddChainItem($arResult['SECTION']['NAME'], '/catalog/'.$arResult['SECTION']['CODE'].'/');
} else {
    $arResult['MAIN_SECTION'] = $arResult['SECTION'];
    $APPLICATION->AddChainItem($arResult['MAIN_SECTION']['NAME'], '/'.$arResult['MAIN_SECTION']['CODE'].'/');
}

$res = CIBlockElement::GetByID($arResult['PROPERTIES']['brend']['VALUE']);
if($brend = $res->GetNext()) {
$arResult['PROPERTIES']['brend']['VALUE'] = $brend['NAME'];
}
$res = CIBlockElement::GetByID($arResult['PROPERTIES']['nextProduct']['VALUE']);
if($product = $res->GetNext()) {
$arResult['PROPERTIES']['nextProduct']['VALUE'] = $product;
}
if(!empty($arResult['PROPERTIES']['nextProduct']['VALUE']['DETAIL_PICTURE'])) {
    $pic = CFile::ResizeImageGet($arResult['PROPERTIES']['nextProduct']['VALUE']['DETAIL_PICTURE'], [620, 620], BX_RESIZE_IMAGE_EXACT);
    $arResult['PROPERTIES']['nextProduct']['VALUE']['DETAIL_PICTURE'] = $pic;
}

$dbList = CIBlockElement::GetProperty($arResult['PROPERTIES']['recommendation']['LINK_IBLOCK_ID'], $arResult['PROPERTIES']['recommendation']['VALUE'], array('id'=>'asc'),array());
$recommendation = [];
while($ar_props = $dbList->GetNext()) {
    $recommendation[$ar_props['CODE']]['SRC'] = CFile::GetPath($ar_props['VALUE']);
    $recommendation[$ar_props['CODE']]['DESCRIPTION'] = $ar_props['DESCRIPTION'];
}
$arResult['PROPERTIES']['recommendation']['VALUE'] = $recommendation;

foreach ($arResult['PROPERTIES']['stick']['VALUE'] as $index => $value) {
    $arResult['PROPERTIES']['stick'][$value] = true;
}
